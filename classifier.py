from sklearn import svm
from sklearn.metrics import precision_recall_fscore_support
import re


def get_data_class(file_name):
	with open(file_name,'r') as f:
		content = f.read()
		pattern  = re.compile('token_.*? INTEGER')
		matches = re.findall(pattern,content)
		token_len = len(matches)

		data = content.split("@DATA")[1].split("\n")
		del data[-1]
		data_list = [line[1:-1].split(',')[:-1] for line in data]
		data_res = []
		for i,line in enumerate(data_list):
			data_res.append([])
			for l in range(0,token_len):
				data_res[i].append(0)

			for val in line:
					for x in range(int(val.split(" ")[1])):
							data_res[i][int(val.split(" ")[0])] += 1

		label_list = [1 if line[1:-1].split(',')[-1].split(' ')[-1]=='yes' else 0 for line in data]
		return data_res,label_list
		
#Split data into features and prediction labels
train_data,train_labels = get_data_class('train.arff')
# print(train_data[1])
print(len(train_data))
print(len(train_labels))
print(len(train_data[1]))
test_data,test_labels = get_data_class('test.arff')

# print(train_data[0:5])
# print(train_labels[0:5])
clf = svm.LinearSVC()
clf.fit(train_data,train_labels)

result = clf.predict(test_data)

print(len(result))

score = precision_recall_fscore_support(test_labels, result, average='macro')

print(score)